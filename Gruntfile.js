module.exports = function(grunt) {

var mozjpeg = require('imagemin-mozjpeg');

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    compass: { //npm to run
      options: { //options for the whole npm
         bundleExec: 'true'
        },
      build: {  //set of tasks to use
        options: {
          environment: 'production'
          }
      },
      dev: {
        options: {
          environment: 'development',
          watch: 'true'
        }
      }
    },
    jshint: {
      all: ['Gruntfile.js', 'js/*.js']
    },
    imagemin: {                        // Task
      options: {                      // Target options
        optimizationLevel: 3,
        svgoPlugins: [{ removeViewBox: false }],
        progressive: true, //change jpegs into progressive jpegs
        interlaced: true, //ensure gifs are interlaced for progressive rendering
        use: [mozjpeg()]
      },
      static: {                          // Target
      files: [
        {src: 'screenshot.png', dest: 'screenshot.png'},
        {src: 'logo.png', dest: 'logo.png'},
      ],
    },
      dynamic: {
        files: [
        {
          expand:true, //Enable dynamic expansion (it is a grunt thing).
          cwd: 'images/', //Src matches are relative to this path
          src: ['**/*.{png,jpg,jpeg,gif}'], //Match these things ** means all sub directories
          dest: 'images/', //Save it in the same space
        },
        ],
      },
    },

});

  // Load the plugin that provides the "compass" task.
  grunt.loadNpmTasks('grunt-contrib-compass');
  // Load the js linting plugin jshint
  grunt.loadNpmTasks('grunt-contrib-jshint');
  // Load the  plugin that optimizes image files
  grunt.loadNpmTasks('grunt-contrib-imagemin');


  // Default task(s).
  grunt.registerTask('default', ['jshint', 'compass:build', 'imagemin' ]);

};
